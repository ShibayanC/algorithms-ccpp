#include <iostream>
using namespace std;

struct Node
{
	int data;
	Node* next;
	Node* front;
};

class DoublyLinkedList
{
	struct Node *head;
public:
	DoublyLinkedList()
	{
		head = NULL;
	}
	void insert(int val);
	void display();
	// void deleteVal(int val);
	void insertVal(int val);
};

void DoublyLinkedList::insert(int val)
{
	struct Node *newNode = new Node;
	newNode -> data = val;
	newNode -> next = NULL;
	newNode -> front = NULL;
	if (head == NULL)
		head = newNode;
	else
	{
		newNode -> next = head;
		head -> front = newNode;
		head = newNode;
	}
}

void DoublyLinkedList::display()
{
	cout<<"Display: ";
	Node *cur, *ptr;
	cur = head;
	while(cur)
	{
		cout<<cur -> data<<" -> ";
		if (cur -> next == NULL)
			ptr = cur;
		cur = cur -> next;
	}
	cout<<"NULL";
	cout<<"\nDisplay Rev: ";
	while(ptr)
	{
		cout<<ptr -> data<<" -> ";
		ptr = ptr -> front;
	}
	cout<<"NULL"<<endl;
	delete (cur);
	delete (ptr);	
}

void DoublyLinkedList::insertVal(int val)
{
	struct Node *newNode = new Node;
	newNode -> data = val;
	newNode -> next = NULL;
	newNode -> front = NULL;
	Node *cur;
	cur = head;
	int flag = 0;
	while(cur)
	{
		if (val < cur -> data)
		{
			flag = 1;
			newNode -> next = cur;
			cur -> front -> next = newNode;
			newNode -> front = cur -> front;
			cur -> front = newNode;
		}
		cur = cur -> next;
	}
	if (flag == 0)
		cout<<"Node can't be inserted";
}
/*
void DoublyLinkedList::deleteVal(int val)
{
	Node *cur, *pos;
	cur = head;
	int flag = 0;
	while(cur)
	{
		if (cur -> data == val)
		{
			flag = 1;
			pos = cur;
			break;
		}
		cur = cur -> next;
	}
	if (flag == 1)
	{
		pos -> next -> front = pos -> front;
		pos -> front -> next = pos -> next;
		cout<<"Deleting: "<<pos -> data;
		delete(pos);
	}
	else
		cout<<"Element not present !";
}
*/
int main()
{
	int arr[] = {12, 24, 36, 48, 60, 72, 84, 96};
	DoublyLinkedList d;	
	for(int i = 0; i < sizeof(arr)/sizeof(arr[0]); i++)
	{
		cout<<arr[i]<<" ";
		d.insert(arr[i]);
		d.display();
	}
	/*
	cout<<endl;	
	d.insert(12);
	// d.display();

	d.insert(24);
	// d.display();

	d.insert(36);
	d.display();
	
	d.insert(48);
	d.display();*/

	d.insertVal(55);
	cout<<endl;
	d.display();

	d.insertVal(80);
	cout<<endl;
	d.display();

	d.insertVal(100);
	cout<<endl;
	d.display();
	/*
	d.deleteVal(36);
	cout<<endl;
	d.display();

	d.deleteVal(66);
	cout<<endl;
	d.display();*/
	cout<<endl;
	return 0;
}
	
		 		

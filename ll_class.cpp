#include <iostream>
using namespace std;

struct Node
{
	int data;
	Node *next;
};

class LinkedList
{
	struct Node *head;
public:
	LinkedList()
	{
		head -> next = NULL;
	}
	void insert();
	// void remove();
	void display();
	void insertAfter();
	void delVal();
};

void LinkedList::insert()
{
	int val;
	cout<<"Enter val: ";
	cin>>val;
	struct Node *newNode = new Node;
	newNode -> data = val;
	newNode -> next = NULL;

	if (head -> next == NULL)
	{
		head -> next = newNode;
	}
	else
	{
		struct Node *tmp;
		tmp = head -> next;
		while (tmp)
		{
			if (tmp -> next == NULL)
			{
				tmp -> next = newNode;
				break;
			}
			tmp = tmp -> next;
		}
	}
}

void LinkedList::display()
{
	struct Node *cur;
	cur = head -> next;
	while (cur)
	{
		cout<<cur -> data<<" -> ";
		cur = cur -> next;
	}
	cout<<"NULL"<<endl;
}

void LinkedList::delVal()
{
	 int val, flag = 0;
	 cout<<"Enter val to be deleted: ";
	 cin>>val;
	 struct Node *ptr = head;
	 while (ptr -> next)
	 {
	 	if (ptr -> next -> data == val)
	 	{
	 		Node *tmp = ptr -> next;
	 		ptr -> next = tmp -> next;
	 		delete tmp;
	 		flag = 1;
	 	}
	 	ptr = ptr -> next;
	 }
	 if (flag == 0)
	 	cout<<val<< " not present in LL";
	 cout<<endl;
}

void LinkedList::insertAfter()
{
	int val, insertVal, flag = 0;
	cout<<"Enter after val: ";
	cin>>val;
	cout<<"Enter val: ";
	cin>>insertVal;
	Node *ptr = head -> next;
	while (ptr -> next)
	{
		if (ptr -> data == val)
		{
			struct Node *newNode = new Node;
			newNode -> data = insertVal;
			newNode -> next = ptr -> next;
			ptr -> next = newNode;
			flag = 1;
			break;
		}
		ptr = ptr -> next;
	}
	if (flag == 0)
		cout<<val<<" not present in the LL";
	cout<<endl;
}
		 
int main()
{
	LinkedList l;
	l.insert();
	l.display();
	l.insert();
	l.display();
	l.insert();
	l.display();
	l.insert();
	l.display();
	l.insert();
	l.display();
	l.insert();
	l.display();

	l.delVal();
	l.display();
	l.delVal();
	l.display();
	
	l.insertAfter();
	l.display();
	l.insertAfter();
	l.display();
	cout<<endl;	
	return 0;
}

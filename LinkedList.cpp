#include <iostream>
using namespace std;

struct Node{
	int val;
	Node *next;
};

class LinkedList{
	private:
		struct Node* head;
	public:
		LinkedList(){
			head = NULL;
		}
		void insert(int data);
		void display();
		int remove();
};

void LinkedList::insert(int data){
	struct Node* newNode = new Node;
	newNode -> val = data;
	newNode -> next = head;
	head = newNode;
}

void LinkedList::display(){
	struct Node* ptr = head;
	cout<<"Display: ";
	while(ptr){
		cout<<ptr -> val<<" -> ";
		ptr = ptr -> next;
	}
	cout<<"NULL"<<endl;
}

int LinkedList::remove(){
	int val;
	struct Node* ptr = head;
	head = head -> next;
	val = ptr -> val;
	delete ptr;
	return val;
}

int main(){
	LinkedList l;
	int arr[] = {12, 25, 59, 23, 20, 64, 37, 28};
	for(int i = 0; i < (sizeof(arr)/sizeof(arr[0])); i++){
		cout<<"Inserting: "<<arr[i]<<endl;
		l.insert(arr[i]);
		l.display();
	}
	cout<<"Removing: "<<l.remove()<<endl;
	l.display();
	cout<<"Removing: "<<l.remove()<<endl;
	l.display();
	return 0;
}

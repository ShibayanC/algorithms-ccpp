#include <iostream>
#include <list>
using namespace std;

class Graph
{
	int V;
	list<int> *adj;
public:
	Graph(int V);
	void addEdge(int v, int w);
	void BFS(int s, bool visited[]);
	bool isConnected();
	Graph getTranspose();
};

Graph::Graph(int V)
{
	this -> V = V;
	adj = new list<int> [V];
}

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
	adj[w].push_back(v);
}

void Graph::BFS(int s, bool visited[])
{
	list<int> q;
	list<int>::iterator i;
	visited[s] = true;
	q.push_back(s);
	while(!q.empty())
	{
		s = q.front();
		q.pop_front();
		for(i = adj[s].begin(); i != adj[s].end(); i++)
		{
			if(!visited[*i])
			{
				visited[*i] = true;
				q.push_back(*i);
			}
		}		
	}
}

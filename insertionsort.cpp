# include<iostream>
using namespace std;

void insertion_sort (int arr[], int length)
{
	int j, temp;
	for (int i = 0; i < length; i++)
	{
		j = i;		
		while (j > 0 && arr[j] < arr[j-1])
		{
			  temp = arr[j];
			  arr[j] = arr[j-1];
			  arr[j-1] = temp;
			  j--;
		}
	}
	cout<<"\nThe sorted array: ";
	for (int i = 0; i<10; i++)
		cout<<"\n"<<arr[i];
}

int main()
{
	int a[10];
	for (int i = 0; i<10; i++)
	{
		cout<<"\nEnter the no. "<<i<<" : ";
		cin>>a[i];
	}
	cout<<"\nThe array entered: ";
	for (int i = 0; i<10; i++)
		cout<<"\n"<<a[i];
	insertion_sort(a, 10);
	return 0;
}

#include <iostream>
using namespace std;

struct Tree
{
	int data;
	Tree *left;
	Tree *right;
	Tree *parent;
};

struct Tree* newTreeNode(int val)
{
	Tree *newTreeNode = new Tree;
	newTreeNode -> data = val;
	newTreeNode -> left = NULL:
	newTreeNode -> right = NULL;
	newTreeNode -> parent = NULL;
}

struct Tree* insertTreeNode(struct Tree* node, int val)
{
	static Tree *p;
	Tree *retNode;
	if (node == NULL)
	{
		retNode = newTreeNode(val);
		retNode -> parent = p;
		return retNode;
	}
	if (val <= node -> data)
	{
		node = p;
		node -> left = insertTreeNode(node -> left);
	}
	else
	{
		node = p;
		node -> right = insertTreeNode(node -> right);
	}
	return node;
}

int treeSize(struct Tree* node)
{
	if (node == NULL) return 0;
	else
		return treeSize(node -> left) + 1 + treeSize(node -> right);
}

int maxDepth(struct Tree *node)
{
	if (node == NULL || (node -> left == NULL && node -> right == NULL))
		return 0;
	int leftDepth = maxDepth(node -> left);
	int rightDepth = maxdepth(node -> right);
	return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
}

int minDepth(struct Tree *node)
{
	if (node == NULL || (node -> left == NULL && node -> right == NULL))
		return 0;
	int leftDepth = minDepth(node -> left);
	int rightDepth = maxDepth(node -> right);
	return leftDepth < rightDepth ? leftDepth + 1 : rightDepth + 1;
}

Tree* maxTree(struct Tree* node)
{
	if (node == NULL) return;
	while(node -> right)
		node = node -> right;
	return node;
}

Tree* minTree(struct Tree* node)
{
	if (node == NULL) return;
	while(node -> left)
		node = node -> left;
	return node;
}

void printTreeInOrder(struct Tree *node)
{
	if (node == NULL) return;
	printTreeInOrder(node -> left);
	cout<<node -> data<<" ";
	printTreeInOrder(node -> right);
}

void printTreePreOrder(struct Tree *node)
{
	if (node == NULL) return;
	cout<<node -> data<<" ";
	printTreePreOrder(node -> left);
	printTreePreOrder(node -> right);
}

void printTreePostOrder(struct Tree *node)
{
	if (node == NULL) return;
	printTreePostOrder(node -> left);
	printTreePostOrder(node -> right);
	cout<<node -> data<<" ";
}

int getLevel(struct Tree *node, int initLevel, int temp)
{
	if (node == NULL) return 0;
	if (node -> data == temp)
		return initLevel;
	else if (temp < node -> data)
		return getLevel(node, initLevel + 1, temp);
	else
		return getLevel(node, initLevel + 1, temp);
}

Tree *lookupNode(struct Tree *node, int temp)
{
	if (node == NULL) return node;
	if (node -> data == temp) return node;
	else
	{
		if (temp < node -> data)
			lookupNode(node -> left, temp);
		else
			lookupNode(node -> right, temp);
	}
}

/* There are three options for deleteKey
	1. a node having no child
	2. a node having only one child (left, right)
	3. a node having both children
*/

void deleteKey(struct Tree* node, int temp)
{
	Tree *tmpNode, *p;
	if (node == NULL) return;
	tmpNode = lookupNode(node, temp);
	
	// 1. node having no child
	if ((node -> left == NULL) && (node -> right == NULL))
	{
		if (tmpNode -> parent)
			p = tmpNode -> parent;
		else if (tmpNode == p -> left)
			p -> left = NULL;
		else
			p -> right = NULL;
		delete tmpNode;
		return;
	}
	
	// 2. Two children - replace it with inorder predecessor and then delete
	if (tmpNode -> left && tmpNode -> right)
	{
		int ch_pred = predecessorInOrder(tmpNode) -> data;
		pred = lookupNode(node, ch_pred);
		if (pred -> parent -> left == pred)
			pred -> parent -> left = NULL;
		else if (pred -> parent -> right == pred)
			pred -> parent -> right = NULL;
		node -> data = pred -> data;
		delete pred;
		return;
	}

	// (this should come at the end) 3. node has two children
	if (tmpNode -> left)
		child = tmpNode -> left;
	else if (tmpNode -> right)
		child = tmpNode -> right;
	p = tmpNode -> parent;
	if (p -> left && p -> left == tmpNode)
		p -> left = child;
	else if (p -> right && p -> right == tmpNode)
		p -> right = child;
	child -> parent = p;
	delete tmpNode;
	return;
}
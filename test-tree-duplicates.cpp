#include <iostream>
using namespace std;

struct Tree
{
	int data;
	int duplicate;
	Tree* left;
	Tree* right;
};

struct Tree* newTreeNode(int val)
{
	struct Tree* newNode = new Tree;
	newNode -> data = val;
	newNode -> duplicate = 1;
	newNode -> left = NULL;
	newNode -> right = NULL;
	return newNode;
}

struct Tree* insertTreeNode(struct Tree* node, int val)
{
	static Tree* retNode;
	if (node == NULL)
	{
		retNode = newTreeNode(val);
		return retNode;
	}
	else
	{
		if (val == node -> data)
			node -> duplicate += 1; 
		else if (val > node -> data)
			node -> right = insertTreeNode(node -> right, val);
		else
			node -> left = insertTreeNode(node -> left, val);
	}
	return node;
}

void printTreeInOrder(struct Tree* node)
{
	if (node == NULL)
		return;
	else
	{
		printTreeInOrder(node -> left);
		cout<<"("<<node -> data<<":"<<node -> duplicate<<")"<<" ";
		printTreeInOrder(node -> right);
	}
}

void printTreePreOrder(struct Tree* node)
{
	if (node == NULL)
		return;
	else
	{
		cout<<"("<<node -> data<<":"<<node -> duplicate<<")"<<" ";
		printTreePreOrder(node -> left);
		printTreePreOrder(node -> right);
	}
}

void printTreePostOrder(struct Tree* node)
{
	if (node == NULL)
		return;
	else
	{
		printTreePreOrder(node -> left);
		printTreePreOrder(node -> right);
		cout<<"("<<node -> data<<":"<<node -> duplicate<<")"<<" ";
	}
}

int main(int argc, char **argv)
{
	int i;
	Tree *root;
	int a[10] = {10, 12, 15, 10, 36, 15, 47, 10, 15, 12};
	root = insertTreeNode(NULL, a[0]);
	for (int i = 1; i< 10; i++)
		insertTreeNode(root, a[i]);
	
	printTreeInOrder(root);
	cout<<endl;
	printTreePreOrder(root);
	cout<<endl;
	printTreePostOrder(root);
	cout<<endl;
	return 0;
}

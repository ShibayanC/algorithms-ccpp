#include <iostream>
#include <string.h>

using namespace std;

class Calculator
{
private:
	int sum = 0;
	int flag = 1;
	char tmp;
public:
	void arraySum(int arr[], int size);
	void compareChar(char *str1, char *str2);
	void revStr(char *Str);
};

void Calculator::arraySum(int arr[], int size)
{
	for(int i = 0; i < size; i++)
	{
		sum = sum + *arr;
		arr++;
	}
	cout<<"Sum: "<<sum<<" *arr: "<<*arr<<endl;
}

void Calculator::compareChar(char *str1, char *str2)
{
	if (strlen(str1) != strlen(str2))
		flag = -1;
	else
	{
		while(*str1 != '\0' && *str2 != '\0')
		{
			if (*str1 != *str2)
				flag = -1;
			str1 ++;
			str2 ++;
		}
	}
	if (flag == -1)
		cout<<"Not same string !";
	else
		cout<<"Same string !";
	cout<<endl;
}

void Calculator::revStr(char *Str)
{
	char tmp;
	char *revStr;
	while(Str)
		Str++;
	Str--;
	if(Str)
	{
		tmp = *Str;
		*revStr = tmp;
		revStr++;
		Str--;
	}
	cout<<"Reverse String: "<<*revStr;
}

Calculator c;

int main(int argc, char **argv)
{
	int size, a[10];
	cin>>size;
	for(int i = 0; i < size; i++)
	{
		cout<<"Enter "<<i+1<<" : ";
		cin>>a[i];
	}
	for(int i = 0; i < size; i++)
		cout<<" "<<a[i];
	cout<<endl;
	c.arraySum(a, size);
	
	char str1[30], str2[30];
	cout<<"Enter str1: ";
	cin>>str1;
	cout<<"Enter str2: ";
	cin>>str2;
	c.compareChar(str1, str2);
	
	char Str[30];
	cout<<"Enter rev str: ";
	cin>>Str;
	c.revStr(Str);
	
	cout<<endl;
	return 0;
}		

#include <iostream>
#include <vector>
//#define N 3
const int D = 5;

using namespace std;

class NaryTree;

class NaryTree
{
public:
	int val;
	vector<NaryTree*> children;

	NaryTree(int v){
		val = v;
		for(int i=0; i<D; ++i)
			this->children.push_back(NULL);
	}


	NaryTree* find(int v){
		if (this->val == v)
			return this;
		else{
			for (int i=0; i<D; ++i){
				NaryTree* retVal = NULL;
				NaryTree* child = this->children[i];
				if (child != NULL){
					retVal= child->find(v);
					if(retVal != NULL)
						return retVal;
				}
			}
		}
		return NULL;
	}
};


int main(){
	NaryTree r1 = NaryTree(1);
	NaryTree r2 = NaryTree(2);
	NaryTree r3 = NaryTree(3);
	NaryTree r4 = NaryTree(4);

	r1.children[0] = &r2;
	r1.children[1] = &r3;
	r1.children[2] = &r4;

	NaryTree* f = r1.find(5);
	if (f == NULL)
		cout << "NOT FOUND"<<endl;
	else
		cout << f->val << endl;
}

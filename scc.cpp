/* To find the connected
components of a undirected
graph, done using DFS */

/* DFS is implemented using
stacks, undirected graph a -> b */

#include <iostream>
#include <stack>
#include <list>

using namespace std;

class Graph
{
	private:
		int V;
		list<int> *adj;
		void DFSUtil(int v, bool visited[]);
		void fillOrder(int v, bool visited[], stack<int> &stk);
	public:
		Graph(int V);
		void addEdge(int v, int w);
		Graph getTranspose();
		int printSCCs();
}

Graph::Graph(int V)
{
	this -> V = V;
	adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
	adj[w].push_back(v);
}

Graph::Graph getTranspose()
{
	Graph g(V);
	for(int v = 0; v < V; v++)
	{
		list<int>::iterator i;
		for(i = adj[v].begin(); i != adj[v].end(); i++)
		{
			g.adj[*i].push_back(v);
		}
	}
	return g;
}

int Graph::printSCCs()
{
	stack<int> stk;
	bool *visited = new bool[V];
	for(int i = 0; i < V; i++)
		visited[i] = false;
	
	for(int i = 0; i < V; i++)
		if(visited[i] == false)
			fillOrder(i, visited, stk);

	Graph gr = getTranspose();
	for(int i = 0; i < V; i++)
		visited[i] = false;
	int count = 0;
	while(stk.empty() == false)
	{
		int v = stk.top();
		stk.pop();
		if (visited[v] == false)
		{
			gr.DFSUtil(v, visited);
			cout<<endl;
		}
		count ++;
	}
	return count;
}

void Graph::DFSUtil(int v, bool visited[])
{
	visited[v] = true;
	cout<<v<<" ";
	list<int>::iterator i;
	for(i = adj[v].begin(); i != adj[v].end(); i++)
		if(!visited[*i])
			DFSUtil(*i, visited);
}

void Graph::fillOrder(int v, bool visited[], stack<int> &stk)
{
	visited[v] = true;
	list<int>::iterator i;
	for(i = adj[v].begin(); i != adj[v].end(); i++)
		if(!visited[*i])
			fillOrder(*i, visited, stk);

	stk.push(v);
}

int main(int argc, char **argv)
{
	Graph g(5);
    g.addEdge(1, 0);
    g.addEdge(0, 2);
    g.addEdge(2, 1);
    g.addEdge(0, 3);
    g.addEdge(3, 4);
 
    cout << "Following are strongly connected components in given graph \n";
    if (g.printSCCs() > 1)
    {
        cout << "Graph is weakly connected.";
    }
    else
    {
        cout << "Graph is strongly connected.";
    }
 
    return 0;
}

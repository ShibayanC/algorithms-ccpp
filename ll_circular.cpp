#include <iostream>
using namespace std;

class CircularLinkedList
{
private:
	struct Node
	{
		int data;
		struct Node *next;
	}*tail;
public:
	CircularLinkedList()
	{
		tail = NULL;
	}
	~CircularLinkedList();
	void createNode(int val);
	void addBegining(int val);
	void addEnd(int val);
	void display();
	void search();
};

void CircularLinkedList::createNode(int val)
{
	struct Node *newNode = new Node;
	newNode -> next = NULL;
	if (tail == NULL)
	{
		tail = newNode;
		newNode -> next = tail;
	}
	else
	{
		newNode -> next = tail -> next;
		tail -> next = newNode;
		tail = newNode;
	}
}
		
void CircularLinkedList::addBegining(int val)
{
	if (tail == NULL)
	{
		cout<<"List not yet created !";
		return;
	}
	struct Node *newNode = new Node;
	newNode -> data = val;
	newNode -> next = tail -> next;
	tail -> next = newNode;
	tail = newNode;
}	
}


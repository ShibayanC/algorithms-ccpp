#include <iostream>
using namespace std;

template <class T>
class LinkedList
{
	private:
		struct node
		{
			T data;
			node *next;
		}*head;
	public:
		LinkedList();
		~LinkedList();
		void add(T d);
		void remove(T d);
		void clear();
		void display(const char* s);
};

template <class T>
LinkedList<T>::LinkedList()
{
	head = NULL;
}

template <class T>
LinkedList<T>::~LinkedList()
{
	node *p, *q;
	p = head;
	if (p == NULL) return;
	while(p){
		q = p -> next;
		delete p;
		p = q;
	}
}

template <class T>
void LinkedList<T>::add(T d)
{
	node *q;
	q = new node;
	q -> data = d;
	q -> next = NULL;
}

template <class T>
void LinkedList<T>::remove(T d)
{
	node *p;
	if (head == NULL) return;
	p = head;
	head = head -> next;
	cout<<"Deleteing: "<<p - > data;
}

template <class T>
void LinkedList<T>::clear()
{
	node *p;
	if (head == NULL) return;
	p = head;
	while (p){
		q = p -> next;
		delete p;
		if (q != head){
			head = NULL;
			return;
		}
		p = q;
	}
}

template <class T>
void LinkedList<T>::display(const char *s)
{
	node *p;
	if (head == NULL) return;
	p = head;
	while(p){
		if (s == "string")
			cout<< p-> data <<endl;
		else
			cout<< p -> data<< endl;
		p = p -> next;
		if (p  != NULL){
			if (p == head) return;
		}
	}
	if (s == "integer") cout<<endl;
}

int main()
{
	LinkedList<string> sList;
	sList.add("Wolf");
	sList.add("Dog");
	sList.add("Cow");

	LinkedList<int> iList;
	iList.add(40);
	iList.add(60);
	iList.add(80);

	return 0;
}

#include <iostream>
#include <vector>
#include <climits>
#include <queue>
using namespace std;
/* Height of a node in a tree,
is the number of edges along the
longest path from the node to a leaf */

/* Depth of a node in a tree,
is the number of edges from the node
to the tree's root node */

struct Tree
{
	char data;
	Tree *left;
	Tree *right;
	Tree *parent;
};

struct Tree *newTreeNode(char val)
{
	Tree *node = new Tree;
	node -> data = val;
	node -> left = NULL;
	node -> right = NULL;
	node -> parent = NULL;
	return node;
};

struct Tree *insertTreeNode(struct Tree *node, char val)
{
	static Tree *p;
	Tree *retNode;
	if (node == NULL)
	{
		retNode = newTreeNode(val);
		retNode -> parent = p;
		return retNode;
	}
	if (val <= node -> data)
	{
		p = node;
		node -> left = insertTreeNode(node -> left, val);
	}
	else
	{
		p = node;
		node -> right = insertTreeNode(node -> right, val);
	}
	return node;
};

void isBST(struct Tree *node)
{
	static int lastData = INT_MIN;
	if (node == NULL) return;
	isBST(node -> left);
	
	/* Check if the given tree is BST */
	if (lastData < node -> data)
		lastData = node -> data;
	else
	{
		cout<<"Not a BST"<<endl;
		return;
	}
	isBST(node -> right);
	return;
}

int treeSize(struct Tree *node)
{
	if (node == NULL) return 0;
	else
		return treeSize(node -> left) + 1 + treeSize(node -> right);
}

int maxDepth(struct Tree *node)
{
	if (node == NULL || (node -> left == NULL && node -> right == NULL))
		return 0;
	
	int leftDepth = maxDepth(node -> left);
	int rightDepth = maxDepth(node -> right);
	
	return leftDepth > rightDepth ?
						leftDepth + 1 : rightDepth + 1;	
}

int minDepth(struct Tree *node)
{
	if (node == NULL || (node -> left == NULL && node -> right == NULL))
		return 0;
	
	int leftDepth = minDepth(node -> left);
	int rightDepth = minDepth(node -> right);
	return leftDepth < rightDepth ? leftDepth + 1 : rightDepth + 1;
}

Tree* minTree(struct Tree *node)
{
	if (node == NULL) return NULL;
	while(node -> left)
		node = node -> left;
	return  node;
}

Tree* maxTree(struct Tree *node)
{
	if (node == NULL) return NULL;
	while(node -> right)
		node = node -> right;
	return node;
}

void printTreeInOrder(struct Tree *node)
{
	if (node == NULL) return;
	printTreeInOrder(node -> left);
	cout<<node -> data<<" ";
	printTreeInOrder(node -> right);
}

void printTreePreOrder(struct Tree *node)
{
	if (node == NULL) return;
	cout<<node -> data<<" ";
	printTreePreOrder(node -> left);
	printTreePreOrder(node -> right);
}

void printTreePostOrder(struct Tree *node)
{
	if (node == NULL) return;
	printTreePostOrder(node -> right);
	printTreePostOrder(node -> left);
	cout<< node -> data <<" ";
}

Tree* lookup(struct Tree *node, char temp)
{
	if (node == NULL) return node;
	if (node -> data == temp) return node;
	else
	{
		if (temp < node -> data)
			lookup(node -> left, temp);
		else
			lookup(node -> right, temp);
	}
}

int getLevel(struct Tree *node, int initLevel, char temp)
{
	if (node == NULL) return 0;
	if (node -> data == temp)
		return initLevel;
	else if (temp < node -> data)
		return getLevel(node, initLevel+1, temp);
	else
		return getLevel(node, initLevel+1, temp);
}

/* In order predecessor is the node which has the next lower key */
Tree *predecessorInOrder(struct Tree *node)
{
	/* If the node has left child, predecessor is the Tree-Maximum */
	if (node -> left != NULL) return maxTree(node -> left);
	Tree *y = node -> parent;
	/* If it does not have a left child,
	predecessor is its first left ancestor */
	while (y != NULL && node == y -> left)
	{
		node = y;
		y = y -> parent;
	}
	return y;
}

/* In order successor is the node which has the next higher key */
Tree *successorInOrder(struct Tree *node)
{
	/* If the node has a right child, successor is tree minimum */
	if (node -> right != NULL) return minTree(node -> right);
	Tree *y = node -> parent;
	while(y != NULL && node == y -> right)
	{
		node = y;
		y = y -> parent;
	}
	return y;
}

void deleteKey(struct Tree* node, char temp)
{
	Tree *tmpNode, *p, *pred, *child;
	// Find out the node first which is to be deleted
	tmpNode = lookup(node, temp);
	
	// If the node is a leaf node (no child), delete it directly
	if ((tmpNode -> left == NULL) && (tmpNode -> right == NULL))
	{
		if (tmpNode -> parent)
			p = tmpNode -> parent;
		else if (tmpNode == p -> left)
			p -> left = NULL;
		else
			p -> right = NULL;
		delete tmpNode;
		return;
	}
		
	// Two children - replace it with the in-order predecessor and delete
	if (tmpNode -> left && tmpNode -> right)
	{
		char ch_pred = predecessorInOrder(tmpNode) -> data;
		pred = lookup(node, ch_pred);
		if (pred -> parent -> left == pred)
			pred -> parent -> left = NULL;
		else if (pred -> parent -> right == pred)
			pred -> parent -> right = NULL;
		node -> data = pred -> data;
		delete pred;
		return;
	}
	
	// If the node has one child, replace the node with its child and then delete
	if (tmpNode -> left)
		child = tmpNode -> left;
	else if (tmpNode -> right)
		child = tmpNode -> right;
	p = tmpNode -> parent;
	if (p -> left && p -> left == tmpNode)
		p -> left = child;
	else if (p -> right && p -> right == tmpNode)
		p -> right = child;
	child -> parent = p;
	delete tmpNode;
	return;
}

int main(int argc, char **argv)
{
	char ch, ch1, ch2;
	Tree *found;
	Tree *succ;
	Tree *pred;
	Tree *ancestor;
	char charArr[9] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'};
	Tree *root = newTreeNode('F');
	insertTreeNode(root, 'B');
	insertTreeNode(root, 'A');
	insertTreeNode(root, 'D');
	insertTreeNode(root, 'C');
	insertTreeNode(root, 'E');
	insertTreeNode(root, 'G');
	insertTreeNode(root, 'I');
	insertTreeNode(root, 'H');
	// To check whether the tree is BST ?
	isBST(root);
	// To find out tree size
	cout<<"Size of the tree: "<<treeSize(root)<<endl;
	// To find out the max depth
	cout<<"Max depth of the tree: "<<maxDepth(root)<<endl;
	// To find out the min depth
	cout<<"Min depth of the tree: "<<minDepth(root)<<endl;
	// To find out the min value
	if (root)
		cout<<"Min. value of the tree: "<<minTree(root) -> data<<endl;
	// To find out the max value
	if (root)
		cout<<"Max. value of the tree: "<<maxTree(root) -> data<<endl;
	
	// To print tree in order
	/* Traverse the left subtree by recursive calling,
	the inorder function; display the root; traverse
	the right subtree by recursively calling the in-order
	function */
	cout<<"In Order: ";
	printTreeInOrder(root);
	cout<<endl;
	
	// To print tree in post order
	/* Diplay the left subtree by recursive call;
	display the right subtree by recursive call;
	display the root */
	cout<<"Post Order: ";
	printTreePostOrder(root);
	cout<<endl;
	
	// To print tree in pre order
	/* Display the data part of the root or current node;
	traverse the left sub tree by recursive call; and then
	traverse the right sub tree  by recursive calling */
	cout<<"Pre Order: ";
	printTreePreOrder(root);
	cout<<endl;	
	
	char tmp;
	cout<<"Enter the char to look-up: ";
	cin>>tmp;
	found = lookup(root, tmp);
	if (found)
		cout<< found -> data <<" is present in the tree at " <<found;
	else
		cout<<tmp <<" is not present in the tree";
	cout<<endl;
	
	// To print the level of a node
	/* Root level is considered to be at 0 level */
	int level = 0;
	cout<<"Enter the char to calculate level: ";
	cin>>tmp;
	level = getLevel(root, 0, tmp);
	cout<<"Level of "<<tmp<<" is: "<<level;
	
	// To delete a particular node in the tree
	cout<<"Enter the char to be deleted: ";
	cin>>tmp;
	deleteKey(root, tmp);
	cout<<endl;
	
	cout<<"Printing tree after deleting "<<tmp<<endl;
	cout<<"In Order: ";
	printTreeInOrder(root);
	cout<<endl;
	
	cout<<"Post Order: ";
	printTreePostOrder(root);
	cout<<endl;
	
	cout<<"Pre Order: ";
	printTreePreOrder(root);
	
	cout<<endl;
	return 0;	
}

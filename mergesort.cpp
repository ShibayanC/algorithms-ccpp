# include<iostream>

using namespace std;

int merge(int a[], int low, int high, int mid);
int mergesort(int a[], int low, int high)
{
	int mid = 0;
	if (low < high)
	{
		mid = (low + high)/2;
		mergesort(a, low, mid);
		mergesort(a, mid+1, high);
		merge(a, low, high, mid);
	}
	return 0;
}

int merge(int a[], int low, int high, int mid)
{
	int i, j, k, c[20];
	i = low;
	k = low;
	j = mid + 1;
	while (i <= mid && j <= high)
	{
		if (a[i] < a[j])
		{
			c[k] = a[i];
			k++;
			i++;
		}
		else
		{
			c[k] = a[j];
			k++;
			j++;
		}
	}
	while ( i <= mid )
	{
		c[k] = a[i];
		k++;
		i++;
	}
	while (j <= mid)
	{
		c[k] = a[j];
		k++;
		j++;
	}
	for (i=low; i<k; i++)
		a[i] = c[i];
}

int main(void)
{
	int a[10];
	for (int i = 0; i<10; i++)
	{
		cout<<"\nEnter the element "<<i<<" : ";
		cin>>a[i];
	}
	cout<<"\nThe array entered: ";
	for (int i = 0; i<10; i++)
		cout<<"\n"<<a[i];
	mergesort(a, 0, 10);
	cout<<"\nThe sorted array: ";
	for (int i = 0; i<10; i++)
		cout<<"\n"<<a[i];
	return 0;
}

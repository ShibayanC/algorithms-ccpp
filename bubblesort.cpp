# include <iostream>

using namespace std;

void insertionsort(int ar[], int size)
{	
	int temp = 0;
	for (int i = 0; i< size; i++)
	{
		for (int j = i+1; j < size; j++)
		{
			if (ar[i] > ar[j])
				{
					temp = ar[j];
					ar[j] = ar[i];
					ar[i] = temp;
				}
		}
	}
	cout<<"\nThe sorted array: ";
	for (int i = 0; i < size; i++)
		cout<<"\n"<<ar[i];
}
int main(void)
{
	int a[10];
	for (int i = 0; i<10; i++)
	{
		cout<<"\nEnter the no. "<<i<<" : ";
		cin>>a[i];
	}
	cout<<"\nThe array entered: ";
	for (int i = 0; i<10; i++)
	{
		cout<<"\n"<<a[i];
	}
	insertionsort(a, 10);
	return 0;	
}

#include <iostream>
using namespace std;

template <typename T>
class List
{
	struct Node
	{
		T data;
		Node *next;
		Node(T d, Node *n = 0):data(d), next(n) {}
	};
	Node *head;
public:
	List(Node *h = 0):head(h) {}
	~List();
	void insert(Node *loc, T d);
	void display();
	void erase(Node *loc);
	void push_back(T d);
	void push_front(T d);
	T pop_back();
	T pop_front();
	Node *search(T d);
};

template <typename T>
List<T>::~List()
{
	Node *tmp;
	while (head)
	{
		tmp = head;
		head = head -> next;
		delete tmp;
	}
}

template <typename T>
void List<T>::push_back(T d)
{
	Node *newNode = new Node(d, 0);
	if (!head)
	{
		head = newNode;
		return;
	}
	newNode -> next = head;
	head = newNode;
	return;
}

template <typename T>
void List<T>::display()
{
	if (!head) return;
	Node *cur = head;
	while (cur)
	{
		cout<<cur -> data<<" "<<endl;		
		cur = cur -> next;
	}
	cout<<endl;
}

template <typename T>
void List<T>::insert(Node *loc, T d)
{
	Node *newNode = new Node(d, 0);
	if (!head)
	{
		head = newNode;
		return;
	}
	if (loc == head)
	{
		push_front(d);
		return;
	}
	Node *cur = head;
	while (cur -> next)
	{
		if (cur -> next == loc)
		{
			newNode -> next = cur -> next;
			cur -> next = newNode;
			return;
		}
		cur = cur -> next;
	}
}

template <typename T>
void List<T>::push_front(T d)
{
	Node *newNode = new Node(d, 0);
	if (!head)
	{
		head = newNode;
		return;
	}
	newNode -> next = head;
	head = newNode;
	return;
}

template <typename T>
void List<T>::erase(Node *loc)
{
	if (loc == head)
	{
		Node *tmp = head;
		head = head -> next;
		delete tmp;
		return;
	}
	Node *cur = head;
	while (cur)
	{
		if (cur -> next == loc)
		{
			cur -> next = loc -> next;
			delete loc;
		}
		cur = cur -> next;
	}
}

template <typename T>
typename List<T>::Node* List<T>::search(T d)
{
	if (!head) return NULL;
	Node *cur = head;
	while (cur)
	{
		if (cur -> data == d) return cur;
		cur = cur -> next;
	}
	return NULL;
}

int main(int argc, char **argv)
{
	List<int> *myList = new List<int>(NULL);
	myList -> push_back(20);
	myList -> push_back(30);
	myList -> push_back(40);
	myList -> push_back(50);
	myList -> display();
	cout << "push_front() 10\n\n";
	myList->push_front(10);
	myList->display();
	
	List<float> *myList1 = new List<float>(NULL);
	myList1 -> push_back(2.05);
	myList1 -> push_back(3.06);
	myList1 -> push_back(4.08);
	myList1 -> push_back(5.011);
	myList1 -> display();
	cout << "push_front() 10.12\n\n";
	myList1 -> push_front(10.12);
	myList1 -> display();
	
	List<char> *myList2 = new List<char>(NULL);
	myList2 -> push_back('a');
	myList2 -> push_back('b');
	myList2 -> push_back('c');
	myList2 -> push_back('d');
	myList2 -> display();
	cout << "push_front() e\n\n";
	myList2 -> push_front('e');
	myList2 -> display();
	
	return 0;
}

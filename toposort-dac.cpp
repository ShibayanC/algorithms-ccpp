#include <iostream>
#include <stack>
#include <list>

using namespace std;

/*
	A acyclic graph or a
	directed acyclic graph
	is a graph which has no
	directed cycle in it. After
	starting from one node
	there is no chance that it will
	reach the same node again
*/
/*	A connected acyclic graph
	is called a tree and a
	disconnected acyclic graph
	is called a forest (collection of trees)
*/
/*	For topological sorting DFS
	is used, and DFS uses stacks
*/
class Graph
{
	private:
		int V;
		list<int> *adj;
		void topologicalSortUtil(int v, bool visited[], stack<int> &stk);
	public:
		Graph(int V);
		void addEdge(int v, int w);
		void topologicalsort();
};

Graph::Graph(int V)
{
	this -> V = V;
	adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
}

void Graph::topologicalsort()
{
	stack<int> stk;
	bool *visited = new bool[V];
	for(int i = 0; i < V; i++)
		visited[i] = false;
	
	// Calling a recursive helper function to store topological sort
	// Starting from all vertices one by one
	
	for(int i = 0; i < V; i++)
		if (visited[i] ==  false)
			topologicalSortUtil(i, visited, stk);

	// Printing the contents of the stack
	while(!stk.empty())
	{
		cout<<stk.top()<<" ";
		stk.pop();
	}	
}

void Graph::topologicalSortUtil(int v, bool visited[], stack<int> &stk)
{
	visited[v] = true;
	list<int>::iterator i;
	for(i = adj[v].begin(); i != adj[v].end(); i++)
		if(!visited[*i])
			topologicalSortUtil(*i, visited, stk);
			
	stk.push(v);
}

int main(int argc, char **argv)
{
	Graph g(6);
	g.addEdge(5, 2);
	g.addEdge(5, 0);
	g.addEdge(4, 0);
	g.addEdge(4, 1);
	g.addEdge(2, 3);
	g.addEdge(3, 1);
	cout<<"Following is the topological sort of the graph: "<<endl;
	g.topologicalsort();
	cout<<endl;
	return 0;
}
	

#include <iostream>
#include <list>
#include <stack>

/* To implement dfs
stacks are used */

/* The graph used is
a unidirected graph a <-> b */
using namespace std;

class Graph
{
	private:
		int V;
		list<int> *adj;
	public:
		Graph(int V);
		void addEdge(int v, int w);
		void DFS(int s, bool visited[]);
		bool isConnected();
		Graph transpose();
};

Graph::Graph(int V)
{
	this -> V = V;
	adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
	adj[w].push_back(v);
}

void Graph::DFS(int s, bool visited[])
{
	stack<int> stk;
	list<int>::iterator i;
	visited[s] = true;
	stk.push(s);
	while(!stk.empty())
	{
		s = stk.top();
		stk.pop();
		for(i = adj[s].begin(); i != adj[s].end(); i++)
		{
			if(!visited[*i])
			{
				visited[*i] = true;
				stk.push(*i);
			}
		}		
	}
}

bool Graph::isConnected()
{
	bool visited[V];
	for(int i = 0; i < V; i++)
		visited[i] = false;
	DFS(0, visited);
	for(int i = 0; i < V; i++)
		if (visited[i] == false)
			return false;
	Graph gr = transpose();
	for(int i = 0; i < V; i++)
		visited[i] = false;
	gr.BFS(0, visited);
    for (int i = 0; i < V; i++)
        if (visited[i] == false)
            return false;
	return true;
}

Graph Graph::transpose()
{
	Graph g(V);
	for(int v = 0; v < V; v++)
	{
		list<int>::iterator i;
		for(i = adj[v].begin(); i != adj[v].end(); i++)
		{
			g.adj[*i].push_back(v);
		}
	}
	return g;
}

int main(int argc, char **argv)
{
	Graph g(4);
	g.addEdge(0, 1);
    	g.addEdge(0, 2);
    	g.addEdge(1, 2);
    	g.addEdge(2, 3);
    	g.addEdge(3, 3);
    	if (g.isConnected())
    		cout<<"The Graph is Connected"<<endl;
    	else
        	cout<<"The Graph is not Connected"<<endl;
     return 0;
}

#include <iostream>
#include <queue>
using namespace std;

struct Node
{
	int data;
	Node *next;
};

class LinkedList
{
	struct Node *head;
	queue<int> myQueue;
public:
	LinkedList()
	{
		head = NULL;
	}
	void insert(int val);
	void display();
	void displayrev();
	void displayrevrecur(head);
};

void LinkedList::insert(int val)
{
	struct Node *newNode = new Node;
	newNode -> data = val;
	newNode -> next = NULL;
	if (head == NULL)
	{
		head = newNode;
		myQueue.push(newNode -> data);
	}
	else
	{
		newNode -> next = head;
		head = newNode;
		myQueue.push(newNode -> data);
	}
}

void LinkedList::display()
{
	cout<<" Display: ";
	Node *cur = head;
	while (cur)
	{
		cout<<cur -> data<<" -> ";
		cur = cur -> next;
	}
	cout<<"NULL\n";
}

void LinkedList::displayrev()
{
	cout<<"Display LL in rev (using queue): ";
	while(!myQueue.empty())
	{
		cout<<myQueue.front()<<" -> ";
		myQueue.pop();
	}
	cout<<"NULL";
}

void LinkedList::displayrevrecur(head)
{
	if (head == NULL)
		return;

	displayrevrecur(head -> next);
	cout<<head -> data<<" -> ";
}

int main(int argc, char **argv)
{
	LinkedList l;
	int arr[] = {12, 34, 56, 43, 87, 24, 90, 66};
	for(int i = 0; i < sizeof(arr)/(sizeof(arr[0])); i++)
	{
		l.insert(arr[i]);
		l.display();
	}
	l.displayrev();
	cout<<"\nDisplaying LL in rev order (using recursion): \n";
	l.displayrevrecur();
	cout<<"NULL";
	cout<<endl;
	return 0;
}

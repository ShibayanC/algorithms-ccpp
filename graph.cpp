#include <iostream>
#include <list>
#include <stack>

using namespace std;
/* BFS uses queue
   DFS uses stack */

class Graph
{
	int V;
	list<int> *adj;
public:
	Graph(int V)
	{
		this -> V = V;
		adj = new list<int>[V];
	}
	void addEdge(int v, int w);
	void BFS(int s);
	void DFS(int s);
};

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
}

void Graph::BFS(int s)
{
	bool *visited = new bool[V];
	for(int i = 0; i < V; i++)
		visited[i] = false;
	
	list<int> queue;
	visited[s] = true;
	queue.push_back(s);
	
	list<int>::iterator i;
	while(!queue.empty())
	{
		s = queue.front();
		cout<<s<<" ";
		queue.pop_front();
		
		for(i = adj[s].begin(); i != adj[s].end(); i++)
		{
			if (!visited[*i])
			{
				visited[*i] = true;
				queue.push_back(*i);
			}
		}
	}
	cout<<endl;
}

void Graph::DFS(int s)
{
	bool *visited = new bool[V];
	for(int i = 0; i < V; i++)
		visited[i] = false;
	
	stack<int> stack;
	visited[s] = true;
	stack.push(s);
	
	list<int>::iterator i;
	while (!stack.empty())
	{
		s = stack.top();
		cout<< s <<" ";
		stack.pop();
		
		for (i = adj[s].begin(); i != adj[s].end(); i++)
		{
			if (!visited[*i])
			{
				visited[*i] = true;
				stack.push(*i);
			}
		}
	}
	cout<<endl;
}

int main(int argc, char **argv)
{
	Graph g(5);
	g.addEdge(0, 1);
	g.addEdge(0, 2);
	g.addEdge(1, 2);
	g.addEdge(2, 0);
	g.addEdge(2, 4);
	g.addEdge(3, 4);
	g.addEdge(1, 3);
	
	cout<<"BFS from vertice 2: ";
	g.BFS(2);
	cout<<"DFS from vertice 2: ";
	g.DFS(2);
	return 0;
}

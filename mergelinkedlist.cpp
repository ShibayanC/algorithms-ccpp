#include <iostream>
using namespace std;

struct Node
{
	int data;
	Node *next;
};

class LinkedList
{
	struct Node *head1, *head2, *head3;
public:
	LinkedList()
	{
		head1 = NULL;
		head2 = NULL;
		head3 = NULL;
	}
	void insert();
	void display();
	void mergelinkedlist();
};

void LinkedList::insert()
{
	int arr1[] = {12, 24, 36, 48, 60, 72};
	int arr2[] = {19, 28, 42, 52};
	for (int i = 0; i < sizeof(arr1)/sizeof(arr1[0]); i++)
	{
		struct Node *newNode = new Node;
		newNode -> data = arr1[i];
		newNode -> next = NULL;
		if (head1 == NULL)
			head1 = newNode;
		else
		{
			newNode -> next = head1;
			head1 = newNode;
		}
	}
	
	for (int i = 0; i < sizeof(arr2)/sizeof(arr2[0]); i++)
	{
		struct Node *newNode = new Node;
		newNode -> data = arr2[i];
		newNode -> next = NULL;
		if (head2 == NULL)
			head2 = newNode;
		else
		{
			newNode -> next = head2;
			head2 = newNode;
		}
	}
}

void LinkedList::display()
{
	Node *cur1, *cur2;
	cur1 = head1;
	cout<<"\nDisplay LL-1: ";
	while(cur1)
	{
		cout<<cur1 -> data<<" -> ";
		cur1 = cur1 -> next;
	}
	cout<<"NULL"<<endl;
	cur2 = head2;
	cout<<"\nDisplay LL-2: ";
	while(cur2)
	{
		cout<<cur2 -> data<<" -> ";
		cur2 = cur2 -> next;
	}
	cout<<"NULL"<<endl;
}

void LinkedList::mergelinkedlist()
{
	cout<<"Merging Linked Lists 1 & 2: ";
	Node *cur1, *cur2;
	cur1 = head1;
	cur2 = head2;
	while ((cur1 != NULL) || (cur2 != NULL))
	{
		struct Node *newNode = new Node;
		if (cur1 -> data < cur2 -> data)
		{
			newNode -> data = cur1 -> data;
			if (head3 == NULL)
			{
				newNode -> next = NULL;
				head3 = newNode;
			}
			else
			{
				newNode -> next = head3;
				head3 = newNode;
			}
			cur1 = cur1 -> next;
		}
		if (cur2 -> data < cur1 -> data)
		{
			newNode -> data = cur2 -> data;
			if (head3 == NULL)
			{
				newNode -> next = NULL;
				head3 = newNode;
			}
			else
			{
				newNode -> next = NULL;
				head3 = newNode;
			}
			cur2 = cur2 -> next;
		}
	}
	
	Node *cur3;
	cur3 = head3;
	cout<<"Merging LL: ";
	while(cur3)
	{
		cout<<cur3 -> data<<" -> ";
		cur3 = cur3 -> next;
	}
	cout<<"NULL"<<endl;
}

int main()
{
	LinkedList l;
	l.insert();
	l.display();
	l.mergelinkedlist();
	return 0;
}

#include <iostream>
#include <vector>
//#define N 3
const int D = 1;

using namespace std;

class NaryTree
{
private:
	vector<NaryTree*> children;
	int num_children;
	int level;
public:
	int val;

	NaryTree(int v){
		val = v;
		num_children = 0;
		level = 0;
	}

	void insert(int v){
		if(this->num_children < D){
			NaryTree* new_child = new NaryTree(v);
			(this->children).push_back(new_child);
			this->num_children ++;
			new_child->level = this->level +1;
		}
		else{
			(this->children[0])->insert(v);
		}
	}

	NaryTree* find(int v){
		if (this->val == v)
			return this;
		else{
			for (int i=0; i<(this->num_children); ++i){
				NaryTree* retVal = NULL;
				NaryTree* child = this->children[i];
				if (child != NULL){
					retVal= child->find(v);
					if(retVal != NULL)
						return retVal;
				}
			}
		}
		return NULL;
	}

	int getLevel(){
		return this->level;
	}
};


int main(){
	NaryTree root = NaryTree(1);
	root.insert(2);
	root.insert(3);
	root.insert(4);

	NaryTree* f = root.find(4);
	if (f == NULL)
		cout << "NOT FOUND"<<endl;
	else
		cout << f->getLevel() << endl;
}

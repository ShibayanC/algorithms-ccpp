#include <iostream>
using namespace std;

struct Node
{
	void *data;
	Node *next;
};

int main()
{
	struct Node *head = new Node;
	int *intData = new int();
	float *floatData = new float();
	char *str = "a";

	*intData = 12;
	*floatData = 44.56;
	
	head -> data = intData;
	struct Node *node1 = new Node;
	node1 -> data = floatData;
	head -> next = node1;
	
	struct Node *node2 = new Node;
	node2 -> data = str;
	node2 -> next = NULL;
	struct Node *tmp = head;
	
	cout<<"Display LL"<<endl;
	if (tmp != NULL)
	{
		cout<<*(int *)(tmp -> data)<<" -> ";
		tmp = tmp -> next;
	}
	if (tmp != NULL)
	{
		cout<<*(float *)(tmp -> data)<<" -> ";
		tmp = tmp -> next;
	}
	if (tmp != NULL)
	{
		cout<<*(char *)(tmp -> data)<<" -> ";
		tmp = tmp -> next;
	}
	cout<<"NULL"<<endl;
	return 0;
}

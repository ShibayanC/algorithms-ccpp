#include <iostream>
using namespace std;

struct Node
{
	int data;
	Node *next;
};

class Stack
{
	struct Node *top;
	int topVal;
public:
	Stack()
	{
		top = NULL;
	}
	void push(int k);
	void pop();
	void display();
	void peek();
};

void Stack::push(int k)
{
	struct Node *newNode = new Node;
	newNode -> data = k;
	newNode -> next = NULL;
	if (top == NULL)
		top = newNode;
	else
	{
		newNode -> next = top;
		top = newNode;
	}
	
}

void Stack::pop()
{
	struct Node *tmp;
	tmp = top;
	top = top -> next;
	cout<<"Popping out: "<<tmp -> data;
	delete (tmp);
}

void Stack::display()
{
	struct Node *tmp;
	tmp = top;
	cout<<" Display: ";
	while (tmp)
	{
		cout<<tmp -> data<<" -> ";
		tmp = tmp -> next;
	}
	cout<<"NULL\n";
}

void Stack::peek()
{
	cout<<"Peek: "<<top -> data;
}

int main(int argc, char **argv)
{
	Stack s;
	int arr[] = {12, 67, 34, 78, 99, 5, 86};
	for (int i = 0; i < sizeof(arr)/sizeof(arr[0]); i++)
	{
		s.push(arr[i]);
		s.display();
	}
	s.pop();
	s.display();
	s.peek();

	s.pop();
	s.display();
	s.peek();
	cout<<endl;
	return 0;
}

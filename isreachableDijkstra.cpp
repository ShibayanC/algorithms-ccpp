#include <iostream>
#include <queue>
#include <list>

/* This code checks whether there
is a path between two given nodes.
The idea is to do a DFS with the
given source node */

/* If the destination node is reached,
path exists and the shortest path is
evaluated */

using namespace std;

class Graph
{
	private:
		int V;
		list<int> *adj;
	public:
		Graph(int V);
		void addEdge(int v, int w);
		bool isReachable(int s, int d);
};

Graph::Graph(int V)
{
	this -> V = V;
	adj = new list<int> [V];
}

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
}

bool Graph::isReachable(int s, int d)
{
	if (s == d)
		return true;
	bool *visited = new bool[V]; // Remember this !
	for(int i = 0; i < V; i++)
		visited[i] = false;
	
	queue<int> q;
	visited[s] = true;
	q.push(s);
	list<int>::iterator i;
	while(!q.empty())
	{
		s = q.front();
		q.pop();
		
		for(i = adj[s].begin(); i != adj[s].end(); i++)
		{
			if (*i == d)
				return true;
			// Else continue with BFS
			if (!visited[*i])
			{
				visited[*i] = true;
				q.push(*i);
			}
		}
	}
	return false;
}

int main(int argc, char **argv)
{
	Graph g(5);
	int u, v, temp;
	g.addEdge(0, 1);
	g.addEdge(0, 2);
	g.addEdge(1, 2);
	g.addEdge(2, 0);
	g.addEdge(2, 3);
	g.addEdge(3, 3);
	cout<<"\nEnter starting node and dest node: ";
	cin>>u >>v;
	if (g.isReachable(u, v))
		cout<<"There is a path from "<<u<<" and "<<v<<endl;
	else
		cout<<"No path exists between "<<u<<" and "<<v<<endl;
	
	temp = u;
	u = v;
	v = temp;
	if (g.isReachable(u, v))
		cout<<"There is a path from "<<u<<" and "<<v<<endl;
	else
		cout<<"No path exists between "<<u<<" and "<<v<<endl;
	
	return 0;
}

#include <iostream>

using namespace std;

struct Node
{
	int data;
	struct Node *next;
};

void addFirstNode(struct Node *head, int val)
{
	head -> data = val;
	head -> next = NULL;
}

void addNode(struct Node *head, int val)
{
	struct Node *newnodeptr = new Node;
	newnodeptr -> data = val;
	newnodeptr -> next = NULL;
	Node *cur = head;
	while(cur)
	{
		if (cur -> next == NULL)
		{
			cur -> next = newnodeptr;
			return;
		}
		cur = cur -> next;
	}
}

void display(struct Node *head)
{
	cout<<"\nDisplaying LL";
	Node *cur = head;
	while(cur)
	{
		cout<<" "<<cur -> data;
		cur = cur -> next;
	}
	cout<<endl;
}

void addFront(struct Node **head, int val)
{
	struct Node *frntNode = new Node;
	frntNode -> data = val;
	frntNode -> next = *head;
	*head = frntNode;
}

void delLL(struct Node **head)
{
	struct Node *tmpnode;
	while(*head)
	{
		tmpnode = *head;
		cout<<"Deleting node: "<<tmpnode -> data<<endl;
		*head = tmpnode -> next;
		delete(tmpnode);
	}
}

void delfront(struct Node *head)
{
	struct Node *frnt = head -> next;
	head -> next = head -> next -> next;
	head -> data = head -> next -> data;
	delete(frnt);
}

int main(int argc, char **argv)
{
	int num, a[10];
	struct Node *head = new Node;
	cout<<"Enter size: ";
	cin>>num;
	for(int i = 0; i < num; i++)
	{
		cout<<"Enter value "<<i+1<<" : ";
		cin>>a[i];
	}
	cout<<"Elements entered: ";
	for(int i = 0; i < num; i++)
		cout<<" "<<a[i];
	for(int i = 0; i < num; i++)
	{
		if (i == 0)
		{
			addFirstNode(head, a[i]);
			display(head);
		}
		else
		{
			addNode(head, a[i]);
			display(head);
		}
	}
	
	int front = 0;
	cout<<"Enter the front node: ";
	cin>>front;
	addFront(&head, front);
	display(head);
	
	cout<<"Deleting front node"<<endl;
	delfront(head);
	display(head);

	cout<<"Deleting linked list"<<endl;
	delLL(&head);
	display(head);
	cout<<endl;
	return 0;
}

#include <iostream>
#include <vector>
#include <queue>
#include <stack>
#include <list>
#include <map>
#include <set>
using namespace std;

int main(int argc, char **argv)
{
	int a[6];
	for(int i = 0; i < 6; i++)
	{
		cout<<"Enter val: ";
		cin>>a[i];
	}
	
	cout<<"The elements present are: ";
	for(int i = 0; i < 6; i++)
		cout<<a[i]<<" ";
	cout<<endl;
	/* STL which use iterators are vector, list, set, multiset, map, priority queue */
	vector<int> myVec;
	vector<int>::iterator it = myVec.begin();
	for(int i = 0; i < 6; i++)
		myVec.push_back(a[i])
	for (it = myVec.begin(); it != myVec.end(); it++)
		cout<<*it<<" ";
	cout<<"Size of myVec: "<<myVec.size();
	
	/*----------------------------------------------------*/

	list<int> myList;
	list<int>::iterator li = myList.begin();
	for(int i = 0; i < 6; i++)
		myList.push_back(a[i]);
	for (li = myList.begin(); li != myList.end(); li++)
		cout<<*li<<" ";
	cout<<"Size of myList: "<<myList.size();
	
	/*---------------------------------------------------*/

	set<int> mySet;
	set<int>::iterator s = mySet.begin();
	for(int i = 0; i < 6; i++)
		mySet.insert(a[i]);
	for(s = mySet.begin(); s != mySet.end(); s++)
		cout<<*s;
	cout<<"Size of mySet: "<<mySet.size();

	multiset<int> myMS;
	multiset<int>::iterator ms = myMS.begin();
	for(int i = 0; i < 6; i++)
		myMS.insert(a[i])
	for(ms = myMS.begin(); ms != myMS.end(); ms++)
		cout<<*ms;
	cout<<"Size of myMS: "<<myMS.size();

	/*----------------------------------------------------*/

	map<char, int> myMap;
	map<char, int>::iterator mp = myMap.begin();
	myMap['A'] = 10;
	myMap['B'] = 20;
	myMap['C'] = 25;
	myMap['D'] = 16;
	myMap['E'] = 7;
	myMap['F'] = 11;
	cout<<"Size of myMap: "<<myMap.size();
	for (mp = myMap.begin(); mp != myMap.end(); mp++)
		cout<<mp -> first<<" -> "<<mp -> second<<endl;
	
	/*---------------------------------------------------*/
	priority_queue<int> pq;
	pq.push(30);
	pq.push(100);
	pq.push(175);
	pq.push(50);
	pq.push(39);
	pq.push(7);
	while (!pq.empty())
	{
		cout<<pq.top()<<" ";      // <-- Notice the difference with normal queue
		pq.pop();
	}
	cout<<endl;
	
	/* Stacks and queues do not need iterators */
	stack<int> myStack;
	for(int i = 0; i < 6; i++)
		myStack.push(a[i]);
	cout<<"Size of stack: "<<myStack.size();
	while (!myStack.empty())
	{
		cout<<myStack.top();
		myStack.pop();
	}
	
	/*--------------------------------------------------*/
	queue<int> myQueue;
	for(int i = 0; i < 6; i++)
		myQueue.push(a[i]);
	cout<<"Size of myQueue: "<<myQueue.size();
	while (!myQueue.empty())
	{
		cout<<myQueue.front()<<endl;
		myQueue.pop();
	}
	
	cout<<endl;
	return 0;
}
